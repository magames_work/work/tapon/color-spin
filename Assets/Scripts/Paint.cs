﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paint : MonoBehaviour
{
    private GameManager gm;
    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    GameObject prevbrush = null;
    private void OnCollisionStay(Collision collision)
    {
        if(collision.GetContact(collision.contactCount - 1).otherCollider.transform.tag == "plate")
        {
            if (prevbrush != null && prevbrush.transform.position != collision.GetContact(collision.contactCount - 1).point)
            {
                GameObject brush = Instantiate(gm.brushPref,
                collision.GetContact(collision.contactCount - 1).point + Vector3.up * 0.2f,
                Quaternion.Euler(new Vector3(gm.plate.transform.eulerAngles.x + 90, gm.plate.transform.eulerAngles.y, gm.plate.transform.eulerAngles.z)),
                gm.canvas.transform);
                prevbrush = brush;
            }
            else if (prevbrush == null)
            {
                GameObject brush = Instantiate(gm.brushPref,
                collision.GetContact(collision.contactCount - 1).point,
                Quaternion.Euler(new Vector3(gm.plate.transform.eulerAngles.x + 90, 0, gm.plate.transform.eulerAngles.z)),
                gm.canvas.transform);
                prevbrush = brush;
            }
        }
    }
}
