﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlate : MonoBehaviour
{
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            float rotZ = 0;
            float rotX = 0;

            rotZ = -Input.GetAxis("Mouse X") * Time.deltaTime * speed;
            rotX = Input.GetAxis("Mouse Y")  * Time.deltaTime * speed;
            

            if (rotZ + transform.eulerAngles.z < 50 | rotZ + transform.eulerAngles.z > 310)
            {
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z + rotZ);
            }
            if (rotX + transform.eulerAngles.x < 50 | rotX + transform.eulerAngles.x > 310)
            {
                transform.eulerAngles = new Vector3(transform.eulerAngles.x + rotX, 0, transform.eulerAngles.z);
            }
        }
    }
}
